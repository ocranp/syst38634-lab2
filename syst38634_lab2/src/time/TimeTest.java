package time;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class TimeTest {

	@Test
	public void testGetTotalMilliSecondsRegular() {

		int totalMilliSeconds = Time.getTotalMilliseconds("01:01:01:01");
		assertTrue("Invalid number of milliseconds", totalMilliSeconds == 01);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMilliSecondsException() {
		int totalMilliSeconds = Time.getTotalMilliseconds("01:01:01:0A");
		fail("Invalid Input");
	}

	@Test
	public void testGetTotalMilliSecondsBooundaryIn() {
		int totalMilliSeconds = Time.getTotalMilliseconds("01:01:01:999");
		assertTrue("Invalid number of milliseconds", totalMilliSeconds == 999);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMilliSecondsBooundaryOut() {
		int totalMilliSeconds = Time.getTotalMilliseconds("01:01:01:1000");
		fail("Invalid number of milliseconds");

	}

	@Test
	public void testGetTotalSecondsRegular() {
		int seconds = Time.getTotalSeconds("01:01:01");
		assertTrue("Invalid Input", seconds == 3661);

	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsException() {
		String time = "01:01:100";
		int seconds = Time.getTotalSeconds(time);
		assertFalse("Time format cannot be greater then 8 characters", time.length() < 8);
	}

	@Test
	public void testGetTotalSecondsBoundaryIn() {
		String time = "01:01:99";
		int seconds = Time.getTotalSeconds(time);
		assertTrue("Time format cannot be greater then 8 characters", time.length() <= 8);
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalSecondsBoundaryOut() {
		String time = "01:01:100";
		int seconds = Time.getTotalSeconds(time);
		fail("Time format cannot be greater then 8 characters");
	}

	@Test(expected = NumberFormatException.class)
	public void testGetTotalMilliSecondsBoundaryIn() {
		String time = "01:01:100:1000";
		int seconds = Time.getTotalMilliseconds(time);
		fail("Time format cannot be greater then 8 characters");
	}

}